const Gpio = require('onoff').Gpio;


const PIN_NUMBER = 26;
const GPIO_NUMBER = 14;
const NOTIFICATION_TYPE = 'motion';


class MotionSensor {

	constructor() {
		this.gpioPort = GPIO_NUMBER;
		this.sensor = new Gpio(this.gpioPort, 'in', 'both');
		this.onMotionCaught = () => {};
	}

	configureGracefulExitHandler() {
		this.off();
			
		console.log('Motion sensor is disconnected');
	}

	startWatch() {
		console.log('Start watching suspicious activity ...');
		
		this.sensor.watch(err => {
			if ( err ) {
				console.log('ERR', err);
				return 1; 
			}
			
			console.log('Motion!');

			//~ return this.onMotionCaught(NOTIFICATION_TYPE);
		});
	}
	
	stopWatch() {
		this.sensor.unwatch();
	}
		
	off() {
		this.sensor.unexport();
	}
	
}


module.exports = MotionSensor;
