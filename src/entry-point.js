const QueryString = require('querystring');
const io = require('socket.io-client');
const DEFAULT_QUERY_STRING_PARAMS = require('./config/defaultRaspberryPiParams');
const EVENT_NAMES = require('./config/event-names');
const SecurityDevice = require('./security-device');


const socket = io(`${process.env.SOCKET_SERVER}?${QueryString.stringify(DEFAULT_QUERY_STRING_PARAMS)}`);

socket.on('connect', () => {
    console.log('Raspberry Pi successfully connected!');
    
    const securityDevice = new SecurityDevice(socket);

    securityDevice.configureSensors();

    socket.on(EVENT_NAMES.CAMERA.PHOTOS.MAKE_PHOTO_REQUEST, () => securityDevice.takePhoto());
    socket.on(EVENT_NAMES.CAMERA.VIDEOS.RECORD_VIDEO_REQUEST, () => securityDevice.recordVideo());
});

socket.on('error', err => {
	console.log(err);
})
