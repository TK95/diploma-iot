const Gpio = require('onoff').Gpio;
const {spawn} = require('child_process');


const PIN_NUMBER = 7;
const GPIO_NUMBER = 4;
const NOTIFICATION_TYPE = 'fire';


class GasSensor {

    constructor() {
        this.gpioPort = GPIO_NUMBER;
        this.onFireCaughtCallback = () => {};
    }
    
    configureGracefulExitHandler() {         
        this.off();

        console.log('Gas sensor is disconnected');
    }

    startWatch() {
        const watchGasProcess = spawn('python', ['gas-sensor.py']);
        
        watchGasProcess.stdout.on('data', (data) => {
            console.log('FIRE!');
            this.onFireCaughtCallback(NOTIFICATION_TYPE);
        });

        watchGasProcess.stderr.on('data', (data) => {
            console.log(`Caught error: ${data}`);
        });

        watchGasProcess.on('close', (code) => {
            console.log(`child process exited with code ${code}`);
            console.log('Stop watching gas level');
        });
    }
    
}


module.exports = GasSensor;


