const EVENT_NAMES = require('./config/event-names');
const MotionSensor = require('./motion-detector');
const CameraSensor = require('./camera-sensor');
const GasSensor = require('./gas-sensor');


class SecurityDevice {
    
    constructor(socket) {
        this.socket = socket;
        this.motionSensor = new MotionSensor();
        this.cameraSensor = new CameraSensor();
        this.gasSensor = new GasSensor();
    }
    
    configureSensors() {
        this.configureMotionSensor();
        this.configureCameraSensor();
        this.configureGasSensor();
        this.configureGracefulExitForAllSensors();
    }
    
    configureMotionSensor() {
        this.motionSensor.onMotionCaught = type => this.socket.emit(EVENT_NAMES.NOTIFICATIONS.NEW_NOTIFICATION, type);
        this.motionSensor.startWatch();
    }
    
    configureCameraSensor() {
        this.cameraSensor.afterPhotoIsTakenCallback = photo => 
			this.socket.emit(EVENT_NAMES.CAMERA.PHOTOS.PREVIEW_IS_READY, photo);
        this.cameraSensor.afterVideoIsRecordedCallback = () => 
			this.socket.emit(EVENT_NAMES.CAMERA.VIDEOS.VIDEO_RECORDED);
		this.cameraSensor.cameraIsBusyCallback = () =>
			this.socket.emit(EVENT_NAMES.CAMERA.DEVICE_IS_BUSY);
    }

    configureGasSensor() {
        this.gasSensor.onFireCaughtCallback = type => this.socket.emit(EVENT_NAMES.NOTIFICATIONS.NEW_NOTIFICATION, type);
        this.gasSensor.startWatch();
    }

    configureGracefulExitForAllSensors() {
        process.on('SIGINT', () => {
            this.motionSensor.configureGracefulExitHandler();
            this.gasSensor.configureGracefulExitHandler();
        });
    }
    
    takePhoto() {
        return this.cameraSensor.takePhoto();
    }

    recordVideo() {
        return this.cameraSensor.startRecordingVideo();
    }
    
}


module.exports = SecurityDevice;
