const {exec, spawn} = require('child_process');
const path = require('path');
const fs = require('fs');


const FILE_NAME = 'preview.jpg';
const TAKE_PHOTO_CLI_COMMAND = `raspistill -n -q 100 -w 500 -h 500 -o ${FILE_NAME}`;
const getRecordVideoCommand = timestamp =>
    `raspivid -t 10000 -w 1280 -h 720 -fps 25 -b 1200000 -p 0,0,1280,720 -o sample--${timestamp}.h264`;


class CameraSensor {
    
    constructor() {
        this.isVideoRecording = false;
        this.isTakingPhoto = false;
        this.afterPhotoIsTakenCallback = () => {};
        this.afterVideoIsRecordedCallback = () => {};
        this.cameraIsBusyCallback = () => {};
    }
    
    takePhoto() {
		if ( ! this.isCameraFree() ) {
			console.log('Camera is busy! Unable to take photo.Try a little later');
			return 1;
		}
		
		this.isTakingPhoto = true;
        return exec(TAKE_PHOTO_CLI_COMMAND, error => {
            if ( error ) {
                console.log('ERR', error);
                this.isTakingPhoto = false;
                return 1;
            }
                        
            return this.readCapturedPreview((error, capturedPreviewBuffer) => {
                if ( error ) {
                    console.log('ERR', error);
                    this.isTakingPhoto = false;
                    return 1;
                }

				console.log('Captured preview!');
				
				const previewInBase64 = capturedPreviewBuffer.toString('base64');
				
			    this.afterPhotoIsTakenCallback(previewInBase64);
				this.isTakingPhoto = false;
			});
        });
    }
    
    startRecordingVideo() {
		if ( ! this.isCameraFree() ) {
			console.log('Camera is busy! Unable to record video.Try a little later')
			return this.cameraIsBusyCallback();
		}
		
		console.log('Starting recording of video');
        this.isVideoRecording = true;
        const timestamp = this.getTimeStamp();
        const recordVideoCommand = getRecordVideoCommand(timestamp);
        return exec(recordVideoCommand, this.stopRecordingVideo.bind(this));
    }
    
    stopRecordingVideo(err) {
        this.isVideoRecording = false;
        
        if ( err) {
            console.log('Error occured during recording of video', err);
            return 1;
        }
        
        console.log('Video successfuly recorded');

        this.afterVideoIsRecordedCallback();
    }
    
    deleteRecentlyCapturedPreview() {
		const capturedPreviewPath = path.join('./', FILE_NAME);
		 
		if( fs.existsSync(capturedPreviewPath) ) {
			return fs.unlinkSync(capturedPreviewPath);
		}
    }
    
    readCapturedPreview(callback) {
        const capturedPreviewPath = path.join('./', FILE_NAME);
        return fs.readFile(capturedPreviewPath, callback);
    }
    
    getTimeStamp() {
        const now = new Date(Date.now()); 
        const dateStringified = now.toLocaleString();
        return dateStringified
            .replace(/\s/g, '_')
            .replace(/\//g, ':')
            .replace(/,/, '_');
    }
    
    isCameraFree() {
		return !this.isVideoRecording && !this.isTakingPhoto;
	}

}

module.exports = CameraSensor;
