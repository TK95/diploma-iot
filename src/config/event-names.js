
module.exports = {
    
    NOTIFICATIONS: {
        NEW_NOTIFICATION: 'notifications/new-notification'
    },
    
    CAMERA: {
		
		DEVICE_IS_BUSY: 'camera/shared/camera-is-busy',
		
        PHOTOS: {
            MAKE_PHOTO_REQUEST: 'camera/photo/make-photo-request',
            PREVIEW_IS_READY: 'camera/photo/photo-preview-is-ready'
        },
        
        VIDEOS: {
            RECORD_VIDEO_REQUEST: 'camera/video/record-video-request',
            VIDEO_RECORDED: 'camera/video/video-recorded'
        }
    }
    
};
